# -*- coding: utf-8 -*-
from flask import Flask, render_template, redirect, url_for, request, session, flash
from werkzeug.utils import secure_filename
from flask_wtf.csrf import CSRFError
from ldapclient import app
from base64 import b64encode
import sha, os, io
from functools import wraps
#from collections import OrderedDict
from randomavatar.randomavatar import Avatar
from PIL import Image
from ldap_helper import *
from utils import *


LDAPCLASSES = {
    'o': ['organization'],
    'ou': ['organizationalUnit'],
    'groupOfNames': ['groupOfNames'],
    'person': ['inetorgperson'],
    'securityObject': ['simpleSecurityObject', 'organizationalRole'],
}

# login required decorator
def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'username' in session:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('login'))
    return wrap

@app.errorhandler(CSRFError)
def handle_csrf_error(e):
    flash(e.description, 'error')
    return redirect(url_for('index'))
    #return logout()

@app.route('/', methods=['GET'])
@app.route('/index', methods=['GET'])
@app.route('/index/<string:dn>/', methods=['GET'])
@login_required
def index(dn = None, error=None):
    if dn:
        session['currentDN'] = dn
    con = bind(app.config['LDAP_SERVER'], session['username'], session['password'])
    
    # attributes of this dn
    try:
        attributes = con.search_s( session['currentDN'], ldap.SCOPE_BASE, '(objectclass=*)' )
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        session['currentDN'] = session['baseDN']
        return render_template('index.html', navItems=getNavItems(session), currentDN=session['currentDN'], attribs=None, entries=None, groups=None)
    attributes = get_search_results(attributes)[0]

    # groups where this dn is a member
    groups = []
    results = con.search_s( session['baseDN'], ldap.SCOPE_SUBTREE, '(&(objectClass=groupOfNames)(member=%s))' % session['currentDN'] )
    results = get_search_results(results)
    for result in results:
        if result:
            groups.append(result.get_dn())
            
    # subtree entries
    entries = {}
    results = con.search_s( session['currentDN'], ldap.SCOPE_ONELEVEL, '(objectclass=*)' )
    results = get_search_results(results)
    entryClass = ''
    for result in results:
        if result.get_attr_values('objectClass')[0] != entryClass:
            entryClass = result.get_attr_values('objectClass')[0]
            if not entries.has_key(entryClass):
                entries[entryClass]=[]
        entries[entryClass].append(result.get_dn())

    template = 'index.html'
    if attributes.get_attr_values('objectClass')[0] == 'inetOrgPerson':
        template = 'user.html'

    return render_template(template, navItems=getNavItems(session, entries), currentDN=session['currentDN'], attribs=attributes, entries=entries, groups=groups)


@app.route('/edit/<string:dn>', methods=['GET', 'POST'])
@login_required
def edit(dn):
    error = None
    session['currentDN'] = dn
    con = bind(app.config['LDAP_SERVER'], session['username'], session['password'])
    if request.method == 'POST':
        # get entry
        try:
            entry = con.search_s( session['currentDN'], ldap.SCOPE_BASE, '(objectclass=*)' )
        except ldap.LDAPError, error:
            flash(unpackError(error), 'error')
            return redirect(url_for('index'))
        attrib = request.form['attrib'].encode("utf-8")
        value = request.form['value'].encode('utf-8')
        mod_attribs=[]

        entry = get_search_results(entry)[0]
        if entry.get_attr_values('objectclass')[0] == 'inetOrgPerson':
            if attrib == 'cn' or attrib == 'sn':
                if attrib == 'cn':
                    dislayname = "%s %s" % (value, entry.get_attr_values('sn')[0].encode('utf-8'))
                if attrib == 'sn':
                    dislayname = "%s %s" % (entry.get_attr_values('cn')[0].encode('utf-8'),value)
                mod_attribs.append(( ldap.MOD_REPLACE, 'displayName', dislayname ))

        mod_attribs.append(( ldap.MOD_REPLACE, attrib, value ))
        try:
            con.modify_s(session['currentDN'], mod_attribs)
        except ldap.LDAPError, error:
            flash(unpackError(error), 'error')
            return redirect(url_for('index'))
            
        # update changed password in session
        if attrib == 'userPassword' and session['currentDN'] == session['username']:
            session['password'] = request.form['value'].encode('utf-8')
        
        if LDAPATTRIBS.has_key(attrib):
            flash("Updated %s" % LDAPATTRIBS[attrib])
        else:
            flash("Updated %s" % attrib)
        return redirect(url_for('index'))
    
    attribute = request.args.get('attrib')
    if not attribute:
        error = 'No attribute defined.'
        flash(unpackError(error), 'error')
        return redirect(url_for('index'))
    try:
        results = con.search_s( session['currentDN'], ldap.SCOPE_BASE, '(objectclass=*)' )
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        return redirect(url_for('index'))
    
    attribs = get_search_results(results)[0]
    if not attribs.has_attribute(attribute):
        error = "Attribute '%s' not found." % attribute
        flash(unpackError(error), 'error')
        return redirect(url_for('index'))
    
    request.form.attrib = attribute
    request.form.value = attribs.get_attr_values(attribute)
    return render_template('edit.html', navItems=getNavItems(session), attribs=attribs, currentDN=session['currentDN'])


@app.route('/edit-members/<string:dn>', methods=['GET', 'POST'])
@login_required
def editMembers(dn):
    error = None
    session['currentDN'] = dn
    con = bind(app.config['LDAP_SERVER'], session['username'], session['password'])
    if request.method == 'POST':
        members=[]
        for element in request.form.items():
            if element[0] == 'csrf_token':
                continue
            if element[1]:
                members.append(element[1].encode("utf-8"))
        if not members:
            try:
                con.delete_s(dn)
                flash("All members removed. Deleted group '%s'" % session['currentDN'])
                session['currentDN'] = getParent(dn)
            except ldap.LDAPError, error:
                flash(unpackError(error), 'error')
                return redirect(url_for('index'))
        else:
            mod_attr = [( ldap.MOD_REPLACE, 'member', members )]
            try:
                con.modify_s(session['currentDN'], mod_attr)
            except ldap.LDAPError, error:
                flash(unpackError(error), 'error')
                return redirect(url_for('index'))
        flash("Group members updated")
        return index(session['currentDN'], error)
    try:
        # attributes of this dn
        results = con.search_s( session['currentDN'], ldap.SCOPE_BASE, '(objectclass=*)' )
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        return redirect(url_for('index'))
    
    attribs = get_search_results(results)[0]
    members = attribs.get_attr_values('member') # members of this groupOfNames
    users = {}
    for member in members:
        users[member] = 1   # 1 because these users are members

    # get local users
    try:
        results = con.search_s( getParent(session['username']), ldap.SCOPE_SUBTREE, '(objectclass=inetorgperson)' )
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        return redirect(url_for('index'))
        
    localusers = [result[0] for result in results]
    for localuser in localusers:
        if not localuser in users:
            users[localuser]=0  # 0 because these users are not members
    
    return render_template('edit_members.html', navItems=getNavItems(session), currentDN=session['currentDN'], users=users.items(), attribs=attribs)


@app.route('/edit-photo/<string:dn>', methods=['GET', 'POST'])
@login_required
def editPhoto(dn):
    error = []
    session['currentDN'] = dn
    con = bind(app.config['LDAP_SERVER'], session['username'], session['password'])
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file', 'error')
            return redirect(request.url)
            
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(filepath)
            # resize image
            photo = Image.open(filepath)
            photo.thumbnail((100, 100), Image.ANTIALIAS)
            # convert to bytearray
            imgByteArr = io.BytesIO()
            photo.save(imgByteArr, format='PNG')
            imgByteArr = imgByteArr.getvalue()
            try:
                con.modify_s(session['currentDN'], [( ldap.MOD_REPLACE, 'jpegPhoto', imgByteArr )])
            except ldap.LDAPError, error:
                flash(unpackError(error), 'error')
                return redirect(url_for('index'))
    try:
        results = con.search_s( session['currentDN'], ldap.SCOPE_BASE, '(objectclass=*)' )
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        return redirect(url_for('index'))
    attribs = get_search_results(results)[0]

    request.form.attrib = 'jpegPhoto'
    return render_template('edit_photo.html', navItems=getNavItems(session), currentDN=session['currentDN'], attribs=attribs, extensions=app.config['PHOTO_EXTENSIONS'])
    

@app.route('/add/<string:dn>', methods=['GET', 'POST'])
@login_required
def add(dn):
    error = None
    con = bind(app.config['LDAP_SERVER'], session['username'], session['password'])
    try:
        results = con.search_s( session['currentDN'], ldap.SCOPE_BASE, '(objectclass=*)' )
    except ldap.LDAPError, error:
        flash(unpackError(error), 'error')
        return redirect(url_for('index'))
    attribs = get_search_results(results)[0]
    
    if request.method == 'POST':
        try:
            request.form['rdn'].decode('ascii')
        except:
            error = "'%s' contains non-ascii character" % request.form['rdn']
            flash(unpackError(error), 'error')
            return render_template('add.html', navItems=getNavItems(session), currentDN=session['currentDN'], attribs=attribs)
        if ' ' in request.form['rdn']:
            error = "'%s' contains white space" % request.form['rdn']
            flash(unpackError(error), 'error')
            return render_template('add.html', navItems=getNavItems(session), currentDN=session['currentDN'], attribs=attribs)

        types = request.form['type']
        rdn = request.form['rdn'].encode("utf-8")

        if types == 'securityObject':
            password = generateRandomPassword()
            ctx = sha.new(password.encode('utf-8'))
            password = "{SHA}" + b64encode(ctx.digest())
            add_record = [
                ('objectclass', LDAPCLASSES['securityObject']),
                ('cn', [rdn]),
                ('description', ["securityObject description"]),
                ('userPassword', [password])
            ]
            dn = 'cn=%s,%s' % (rdn, session['currentDN'])
            
        if types == 'o':
            add_record = [
                ('objectclass', ['organization']),
                ('o', [rdn]),
                ('description', ["A description of the organization '%s'" % rdn])
            ]
            dn = 'o=%s,%s' % (rdn, session['currentDN'])
                                  
        if types == 'ou':
            add_record = [
                ('objectclass', ['organizationalUnit']),
                ('ou', [rdn]),
                ('description', ["A description of the unit '%s'" % rdn])
            ]
            dn = 'ou=%s,%s' % (rdn, session['currentDN'])
            
        if types == 'groupOfNames':
            add_record = [
                ('objectclass', LDAPCLASSES['groupOfNames']),
                ('cn', [rdn]),
                ('member', [session['username'].encode("utf-8")]),
                ('description', ["A description of the group '%s'" % rdn]),
                ('owner', [session['username'].encode("utf-8")])
            ]
            dn = 'cn=%s,%s' % (rdn, session['currentDN'])
            
        if types == 'person':
            password = generateRandomPassword()
            ctx = sha.new(password.encode('utf-8'))
            password = "{SHA}" + b64encode(ctx.digest())
            avatar = Avatar(rows=10, columns=10)
            image_byte_array = avatar.get_image(string=rdn, width=100, height=100, pad=0)
            add_record = [
                ('objectclass', LDAPCLASSES['person']),
                ('uid', [rdn]),
                ('userPassword', [password.encode("utf-8")]),
                ('mail', ['%s@example.com' % rdn]),
                ('cn', [rdn]),
                ('sn', [rdn]),
                ('preferredLanguage', ['en']),
                ('o', ['Unknown organization']),
                ('displayName', ['%s %s' % (rdn, rdn)]),
                ('description', ['I am %s. This is my short biography.' % rdn]),
                ('jpegPhoto', [image_byte_array] ),
                ('pager', ['1 GB'])
            ]
            dn = 'uid=%s,%s' % (rdn, session['currentDN'])

        try:
            con.add_s(dn, add_record)
        except ldap.LDAPError, error:
            flash(unpackError(error), 'error')
            return render_template('add.html', navItems=getNavItems(session), currentDN=session['currentDN'], attribs=attribs)

        session['currentDN'] = dn
        if types == 'person':
            flash("Please update user information <i class='fa fa-coffee' aria-hidden='true'></i>")
        else:
            flash("Added %s '%s'" % (types, rdn))
        return index(session['currentDN'], error)

    return render_template('add.html', navItems=getNavItems(session), currentDN=session['currentDN'], attribs=attribs)


@app.route('/search', methods=['GET', 'POST'])
@login_required
def search(error=None):
    error = None
    session['currentDN'] = app.config['BASE_DN']
    con = bind(app.config['LDAP_SERVER'], session['username'], session['password'])
    if request.method == 'POST':
        value = request.form['value']
        if not value:
            return render_template('search.html', navItems=getNavItems(session), currentDN=session['currentDN'])
        try:
            results = con.search_s( session['currentDN'], ldap.SCOPE_SUBTREE, '(&(objectClass=inetOrgPerson)(uid=%s))' % value )
        except ldap.LDAPError, error:
            flash(unpackError(error), 'error')
            return render_template('search.html', navItems=getNavItems(session), currentDN=session['currentDN'])
        if results:
            flash("Found '%s'" % value)
            attribs = get_search_results(results)[0]
            return index( attribs.get_dn() )
        else:
            flash("Cannot find '%s'. <i class='fa fa-meh-o' aria-hidden='true'></i>" % value, 'warning')

    request.form.attrib = 'uid'
    request.form.value = ''
    return render_template('search.html', navItems=getNavItems(session), currentDN=session['currentDN'])


@app.route('/delete/<string:dn>', methods=['GET', 'POST'])
@login_required
def delete(dn):
    error = None
    if dn == session['username']:
        flash("Please don't delete yourself. <i class='fa fa-hand-spock-o' aria-hidden='true'></i>", 'error')
        return redirect(url_for('index'))
        
    con = bind(app.config['LDAP_SERVER'], session['username'], session['password'])
    results = con.search_s( session['currentDN'], ldap.SCOPE_BASE, '(objectclass=*)' )
    attribs = get_search_results(results)[0]
    if request.method == 'POST':
        if attribs.has_attribute('member') and attribs.get_attr_values('member'):
            flash('Remove all members first', 'error')
            return redirect(url_for('index'))
        try:
            con.delete_s(dn)
            session['currentDN'] = getParent(dn)
        except ldap.LDAPError, error:
            flash(unpackError(error), 'error')
            return redirect(url_for('index'))
        flash("Deleted '%s'" % dn)
        return redirect(url_for('index'))
       
    return render_template('delete.html', navItems=getNavItems(session), currentDN=session['currentDN'], attribs=attribs)
        

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if not request.form['username'] and not request.form['password']:
            flash("Credentials needed", "error")
            return render_template('login.html', navItems=getNavItems(session), base_dn=app.config['BASE_DN'])
        con = bind(app.config['LDAP_SERVER'], request.form['username'], request.form['password'])
        # ldap server returns a SimpleLDAPObject object after a successful login
        if con.__class__.__name__ != "SimpleLDAPObject":
            flash(con, 'error')
        else:
            session['username'] = request.form['username']
            session['password'] = request.form['password']
            session['currentDN'] = request.form['username']
            session['baseDN'] = app.config['BASE_DN']
            session['tree'] = {}
            session['showTree'] = True
            flash("Welcome! <i class='fa fa-smile-o'></i>")
            return redirect(url_for('index'))
    return render_template('login.html', navItems=getNavItems(session), base_dn=app.config['BASE_DN'])


@app.route('/logout', methods=['GET'])
@login_required
def logout():
    session.pop('username', None)
    flash("Good bye.")
    return redirect(url_for('index'))
